/* License (BSD Style License):
 * Copyright (c) 2009 - 2013
 * Software Technology Group
 * Department of Computer Science
 * Technische Universität Darmstadt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  - Neither the name of the Software Technology Group or Technische
 *    Universität Darmstadt nor the names of its contributors may be used to
 *    endorse or promote products derived from this software without specific
 *    prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package de.tud.cs.st
package bat
package resolved

import analyses.{ Analysis, AnalysisExecutor, BasicReport, Project }
import java.net.URL
import instructions._
import scala.xml._
/**
 * checks occurances of observer related code
 *
 * @author Linus Armakola
 * @author Michael Eichberg
 */
object CountObservables extends AnalysisExecutor {
  /*

   * 1.1 wie oft werden observerobjekte instan observerfunktionen (notify update etc.) aufgerufen und observerobjekte übergeben
   * 1.2 wie oft wird eventobjekt etc. erstellt
   * 2. alle klassen die sie benutzen
   * alle listener und observer identifizieren
   * array oder list mit listenern drinne
   * xml ausgabe mit möglichst vielen zahlen
   * 3. Markdown zur dokumentation anlegen
   */

  val analysis = new Analysis[URL, BasicReport] {

    def description: String = "Gives some details on how Observer Pattern is used in project."
    		
      
      
    def countEventObjectInvokes(project: Project[URL]): Iterable[String] = {
      
      val observers =project.classFiles.filter(_.thisType.fqn.endsWith("Listener")).map(_.thisType)
      val allObservers = observers.flatMap(project.classHierarchy.allSubtypes(_)).toSet
      println(allObservers.mkString("\n"))
      val eventObjects = project.classHierarchy.allSubtypes(ObjectType("java/util/EventObject"))
      for {
        classFile ← project.classFiles
        if classFile.thisType.fqn.startsWith("de/tud/cs/se/flashcards/")
        method <- classFile.methods
        if method.body.isDefined
        code = method.body.get
        eventObjectPC <- code.collectWithIndex({ case (pc, NEW(ot)) if eventObjects.contains(ot) => pc })

      } yield {
        (classFile.thisType.fqn + "\t"
          + method.toJava + "\t"
          + code.lineNumberTable.map(_.lookupLineNumber(eventObjectPC).getOrElse(-1)))
      }
    }

    def countEventListenersInvokes(project: Project[URL]): Iterable[String] = {
      val eventListeners = project.classHierarchy.allSubtypes(ObjectType("java/util/EventListener"))
      for {
        classFile ← project.classFiles
        if classFile.thisType.fqn.startsWith("de/tud/cs/se/flashcards/ui")
        method <- classFile.methods
        if method.body.isDefined
        code = method.body.get
        eventListenerPC <- code.collectWithIndex({ case (pc, NEW(ot)) if eventListeners.contains(ot) => pc })
      } yield {
        (classFile.thisType.fqn + "\t"
          + method.toJava + "\t"
          + code.lineNumberTable.map(_.lookupLineNumber(eventListenerPC).getOrElse(-1)))
      }
    }

    def countNotifications(project: Project[URL]): Iterable[String] = {
      for {
        classFile <- project.classFiles
        if classFile.thisType.fqn.startsWith("de/tud/cs/se/flashcards/ui/FlashcardsEditor")
        method <- classFile.methods
        if method.body.isDefined
        code = method.body.get
        instruction <- code.instructions
        pc <- code.collectWithIndex({ case (pc, instruction) if true => pc })
      } yield {
        (classFile.thisType.fqn + "\t"
          + method.toJava + "\t"
          + code.lineNumberTable.map(_.lookupLineNumber(pc).getOrElse(-1)))
      }
    }

    def checkFields(project: Project[URL]): Iterable[String] = {
      val eventListeners = project.classHierarchy.allSubtypes(ObjectType("java/util/EventListener"))
      val eventObjects = project.classHierarchy.allSubtypes(ObjectType("java/util/EventObject"))
      for {
        classFile <- project.classFiles
        if classFile.thisType.fqn.startsWith("de/tud/cs/se/flashcards/")
        field <- classFile.fields
        if field.fieldType.isObjectType && (eventObjects.contains(field.fieldType.asObjectType) || eventListeners.contains(field.fieldType.asObjectType))
        method <- classFile.methods
        if method.body.isDefined
        code = method.body.get
      } yield {
        (field.name + "\t" + method.name + "\t" + field.toJavaSignature + "\t" + classFile.thisType.fqn)
      }
    }

    def analyze(project: Project[URL], parameters: Seq[String]) = {
      val fields = checkFields(project)
      val eventListenerInvokes = countEventListenersInvokes(project)
      val eventObjectInvokes = countEventObjectInvokes(project)
      
      val xml = for {
        event <- eventObjectInvokes
      } yield {
        <event>event</event> 
      }
	   println("XML BLABLA" + xml.)
      

      BasicReport(
        "\n\n========================================================\n\n"
          + "EventObject instance creations: \n\t"
          + eventObjectInvokes.toSet.mkString("\n\t")
          + "\n\n========================================================\n\n"
          + "EventListener instance creations: \n\t"
          + eventListenerInvokes.toSet.mkString("\n\t")
          + "\n\n========================================================\n\n"
          + "Operations on either java.util.EventListener or java.util.EventObject heirs: \n\t"
          + fields.toSet.mkString("\n\t") + "\n"
          + "\n\n========================================================\n\n"
          + "Number of EventObject instance creations " + eventObjectInvokes.size + "\n\n"
          + "Number of EventListener instance creations " + eventListenerInvokes.size + "\n\n"
          + "Number of Operations on either java.util.EventListener or java.util.EventObject heirs: " + fields.size + "\n\n")
    }
  }
}